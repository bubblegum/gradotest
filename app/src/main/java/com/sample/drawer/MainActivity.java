package com.sample.drawer;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeader;
import com.sample.drawer.adapters.NavigationAdapter;
import com.sample.drawer.fragments.Fragment1;
import com.sample.drawer.models.NavigationItem;
import com.sample.drawer.utils.Utils;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity {

    NavigationLoader navigationLoader;
    private ProgressDialog pDialog;
    String navigationTree;
    TextView infoLog;

    private Drawer.Result drawerResult = null;
    private AccountHeader.Result headerResult = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Для Activity с боковым меню ставьте эту тему,
        // для Activity без бокового меню ставьте тему AppThemeNonDrawer (она прописана по умолчанию в манифесте кстати)
        // иначе будет "сползать" ActionBar
        // Темы находятся в styles.xml
        setTheme(R.style.AppThemeDrawer);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        infoLog = (TextView) findViewById(R.id.infoLog);
        // init Drawer & Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        headerResult = Utils.getAccountHeader(MainActivity.this, savedInstanceState);
        drawerResult = Utils.createCommonDrawer(MainActivity.this, toolbar, headerResult);
        drawerResult.setSelectionByIdentifier(1, false); // Set proper selection

        // Покажем drawer автоматически при запуске
        //drawerResult.openDrawer();

        final FragmentManager fm = getSupportFragmentManager();
        final FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.frame_container, new Fragment1())
                .commit();

        Log.i("MainActivity", "MainActivity loaded");

        // Construct the data source
        ArrayList<NavigationItem> arrayOfItems = new ArrayList<NavigationItem>();
        // Create the adapter to convert the array to views
        NavigationAdapter adapter = new NavigationAdapter(this, arrayOfItems);
        // Add item to adapter
        NavigationItem newNavigationItem = new NavigationItem("Nathan", "San Diego", "category");
        adapter.add(newNavigationItem);
        newNavigationItem = new NavigationItem("Nathan", "San Diego", "category");
        adapter.add(newNavigationItem);
        newNavigationItem = new NavigationItem("Nathan", "San Diego", "category");
        adapter.add(newNavigationItem);


        // Attach the adapter to a ListView
        ListView listView = (ListView) findViewById(R.id.navigationItems);
        listView.setAdapter(adapter);
        //navigationLoader = new NavigationLoader();
        //navigationLoader.execute();
    }

    @Override
    public void onBackPressed() {
        if (drawerResult.isDrawerOpen()) {
            // Закрываем меню, если оно показано и при этом нажата системная кнопка "Назад"
            drawerResult.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }


    class NavigationLoader extends AsyncTask<Void, Void, Void> {

        String result = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            // Making a request to url and getting response
            navigationTree = sh.makeServiceCall("http://api.gradoteka.ru/navigation.json", ServiceHandler.GET);

            Log.d("Response: ", "> " + navigationTree);

            if (navigationTree != null) {
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();

            infoLog.setText("End - " + navigationTree );
        }
    }
}
