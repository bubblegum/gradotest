package com.sample.drawer.models;

/**
 * Created by stoderevsky on 30.07.15.
 */
public class NavigationItem {
    public String name;
    public String description;
    public String type;

    public NavigationItem(String name, String description, String type) {
        this.name = name;
        this.description = description;
        this.type = type;
    }

}
