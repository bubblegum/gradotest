package com.sample.drawer.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.sample.drawer.R;
import com.sample.drawer.models.NavigationItem;

import java.util.ArrayList;

/**
 * Created by stoderevsky on 30.07.15.
 */
public class NavigationAdapter extends ArrayAdapter<NavigationItem> {

    private static class ViewHolder {
        TextView name;
        TextView type;
    }


    public NavigationAdapter(Context context, ArrayList<NavigationItem> items) {
        super(context, 0, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        NavigationItem item = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_navigation, parent, false);

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item_navigation, parent, false);
            viewHolder.name = (TextView) convertView.findViewById(R.id.navigationName);
            viewHolder.type = (TextView) convertView.findViewById(R.id.navigationType);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        // Populate the data into the template view using the data object
        viewHolder.name.setText(item.name);
        viewHolder.type.setText(item.type);
        // Return the completed view to render on screen
        return convertView;

    }
}